package simulator

import "fmt"

/**
 * Place the Robot on the Gameboard (try)
 *
 * Params
 * m string => a to-be-parsed command
 * g => the gameboard (value)
 * r => the Robot (reference)
 **/
func tryPlace(m string, g gameboard, r *Robot) {

	check := false

	// parse the command
	check, coord, face := parsePlace(m)

	if check {
		// check positioning on each axis
		xy := [2]string{"x", "y"}
		z := ""
		for i := 0; i < len(xy); i++ {

			if !check {
				break
			}

			z = xy[i]
			_, check = checkMove(g.area["min"][z], coord[i], g.area["min"][z], g.area["max"][z])
		}
	}

	// if check succeded then we place it into the wanted coordiantes and face ...
	if check {
		// we can turn the robot ON
		r.Position["x"] = coord[0]
		r.Position["y"] = coord[1]
		r.Face = face
		r.Ready = check
	}
}

/**
 * Move the Robot (try)
 *
 * Params
 * g => the gameboard (value)
 * r => the Robot (reference)
 **/
func tryMove(g gameboard, r *Robot) {
	mv, ax := movesRules()

	current := r.Position[ax[r.Face]]
	units := mv["toy"] * mv[r.Face]
	min := g.area["min"][ax[r.Face]]
	max := g.area["max"][ax[r.Face]]
	toBe, check := checkMove(current, units, min, max)

	if check {
		r.Position[ax[r.Face]] = toBe
	}
}

/**
 * Rotate/ Turn the Face of the Robot
 *
 * Params
 * m string => a to-be-used-as-switch command
 * r => the Robot (reference)
 **/
func rotate(m string, r *Robot) bool {

	dr, fc, lm := spinRules()
	toFace := fc[r.Face] + dr[m]

	if toFace < lm["min"] {
		toFace = lm["max"]
	}
	if toFace > lm["max"] {
		toFace = lm["min"]
	}

	check := false
	newFace := "DOWN"
	// Loop over the map.
	for key, value := range fc {
		if value == toFace {
			newFace = key
			check = true
			break
		}
	}

	r.Face = newFace
	return check
}

/**
 * Report the Robot Position
 *
 * Params
 * r => the Robot (value)
 **/
func report(r Robot) /*string*/ {
	fmt.Print(r.Position["x"], ",", r.Position["y"], ",", r.Face)
	fmt.Print("\n")
}
