package simulator

import (
	// "fmt"
	"strconv"
	"strings"
)

/**
 * Servo for check the move or abort it
 *
 * Params
 * c int => current position
 * n int => new, desidered, position
 * min int => board minimum limit
 * max int => board maximum limit
 **/
func checkMove(c, n, min, max int) (int, bool) {
	toBe := c + n
	res := true

	if toBe < min || toBe > max {
		toBe = c
		res = false
	}

	return toBe, res
}

/**
 * Servo to parse the position from the place command
 *
 * Params
 * m string => the command to be parsed and executed
 **/
func parsePlace(m string) (bool, []int, string) {

	check := false
	failPos := []int{-99, -99}
	failFace := "DOWN"

	// Parsing position coordiantes for PLACE command
	a := strings.Split(m, " ")
	if len(a) < 2 {
		return check, failPos, failFace
	}

	b := strings.Split(a[1], ",")
	if len(b) < 3 {
		return check, failPos, failFace
	}

	// check if command has valid (int) coordinates
	toCoord := b[0:2]
	check, coord := parseCoords(toCoord)
	if !check {
		return check, failPos, failFace
	}

	// check if command has valid face
	toFace := b[2]
	_, faces, _ := spinRules()
	check = findFace(toFace, faces)

	return check, coord, toFace
}

// Parse coordinates => string to int
func parseCoords(c []string) (bool, []int) {

	check := true
	coord := []int{}

	var j int
	for _, i := range c {
		_, err := strconv.Atoi(i)
		if err != nil {
			check = false
			j = -999
		} else {
			j, _ = strconv.Atoi(i)
		}
		coord = append(coord, j)
	}
	return check, coord
}

// Check if the desidered face value exists in rules/settings
func findFace(t string, m map[string]int) bool {

	check := false

	for key := range m {
		if key == t {
			check = true
			break
		}
	}

	return check
}
