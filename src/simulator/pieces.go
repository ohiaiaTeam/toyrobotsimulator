package simulator

// A good robot always knows
type Robot struct {
	// Its position
	Position map[string]int
	// indicate where its face is looking torward North/South/East/West
	Face string
	// if its ready to be moved or not
	Ready bool
}

// A game board is needed
type gameboard struct {
	// Its "origin" corner coordinates
	corner map[string]string
	// It has an area
	area map[string]map[string]int
}
