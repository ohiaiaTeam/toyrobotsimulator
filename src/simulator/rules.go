package simulator

// Game Field
func fieldSettings() (map[string]string, map[string]map[string]int) {
	origin := map[string]string{"x": "WEST", "y": "SOUTH"}
	area := map[string]map[string]int{"min": {"x": 0, "y": 0}, "max": {"x": 5, "y": 5}}
	return origin, area
}

// Robot OffGame
func robotInit() (map[string]int, string, bool) {
	position := map[string]int{"x": -99, "y": -99}
	face := "DOWN"
	ready := false
	return position, face, ready
}

// Allowed Moves
func movesRules() (map[string]int, map[string]string) {
	moves := map[string]int{"toy": 1, "NORTH": +1, "EAST": +1, "SOUTH": -1, "WEST": -1}
	axis := map[string]string{"NORTH": "x", "EAST": "y", "SOUTH": "x", "WEST": "y"}
	return moves, axis
}

// Rotation
func spinRules() (map[string]int, map[string]int, map[string]int) {
	direction := map[string]int{"RIGHT": 1, "LEFT": -1}
	facing := map[string]int{"NORTH": 0, "EAST": 1, "SOUTH": 2, "WEST": 3}
	limits := map[string]int{"min": 0, "max": 3}
	return direction, facing, limits
}
