package simulator

import (
	"bufio"
	"log"
	"os"
	"strings"
)

/**
 * The Game Simulation
 *
 * Params
 * f => a verified (existing) input file
 **/
func Simulation(f *os.File) Robot {
	// Scan the file
	scanner := bufio.NewScanner(f)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Import the Field settings as game's rules
	origin, area := fieldSettings()
	// Create the Square Game Field
	g := gameboard{origin, area}

	// Settings to initialize the robot
	pos, face, ready := robotInit()
	// Init the robot
	r := Robot{pos, face, ready}

	// Read line by line the input file and play the game!
	for scanner.Scan() {
		control(scanner.Text(), g, &r)
	}

	return r
}

/**
 * Control the Robot
 *
 * Params
 * m string => the command to be parsed and executed
 * g => the gameboard (value)
 * r => the Robot (reference)
 **/
func control(m string, g gameboard, r *Robot) {

	m = strings.ToUpper(m)
	m = strings.TrimSpace(m)

	if strings.HasPrefix(m, "PLACE") {

		// . The first valid command to the robot is a PLACE command
		tryPlace(m, g, r)
	} else {

		// . A robot that is not on the table can choose to ignore the
		// 	 MOVE, LEFT, RIGHT and REPORT commands.
		if r.Ready {
			switch m {

			// . MOVE will move the toy robot one (variable in settings) unit
			//	forward in the direction it is currently facing.
			case "MOVE":
				tryMove(g, r)

			// . LEFT and RIGHT will rotate the robot 90 degrees in the specified
			//   direction without changing the position of the robot.
			case "LEFT", "RIGHT":
				rotate(m, r)

			// . REPORT will announce the X,Y and orientation of the robot.
			case "REPORT":
				report(*r)
			}
		}

	}
}
