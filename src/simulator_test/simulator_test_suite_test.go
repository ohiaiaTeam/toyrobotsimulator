package simulator_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestSimulatorTest(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "SimulatorTest Suite")
}
