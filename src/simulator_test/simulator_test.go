package simulator_test

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "simulator"
)

func TestSimulator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Toy Robot Simulator Suite")
}

var _ = Describe("Robot Simulator", func() {

	Context("during the game", func() {

		Context("the Input", func() {
			Specify("that invalid moves are ignored", func() {
				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,0,NORTH \n JUMP"
				createTheInputFile(name, body)

				position := map[string]int{"x": 0, "y": 0}
				face := "NORTH"
				ready := true

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)
				Expect(r.Position).Should(BeEquivalentTo(position))
				Expect(r.Face).Should(BeEquivalentTo(face))
				Expect(r.Ready).Should(BeEquivalentTo(ready))

			})
		})

		Context("putting the Robot place", func() {

			It("should be able to be placed correctly", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,0,NORTH"
				createTheInputFile(name, body)

				position := map[string]int{"x": 0, "y": 0}
				face := "NORTH"
				ready := true

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)
				Expect(r.Position).Should(BeEquivalentTo(position))
				Expect(r.Face).Should(BeEquivalentTo(face))
				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the face value is not in an acceptable range", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,0,UP "
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the X coord is not in an acceptable range", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 9999,0,NORTH "
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the Y coord is not in an acceptable range", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,9999,NORTH "
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the X coord is not a number", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE A,0,NORTH "
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the Y coord is not a number", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,A,NORTH"
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should fail when the command is incomplete", func() {

				name := "tmp/canBePlaced.txt"
				body := "PLACE 0,0 "
				createTheInputFile(name, body)

				ready := false

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)

				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})
		})

		Context("making the robot turn", func() {

			It("should be able to spin looplessly to the left", func() {

				name := "tmp/canSpin.txt"
				body := "PLACE 0,0,NORTH \n LEFT \n  LEFT \n  LEFT \n  LEFT \n  LEFT \n"
				createTheInputFile(name, body)

				position := map[string]int{"x": 0, "y": 0}
				face := "WEST"
				ready := true

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)
				Expect(r.Position).Should(BeEquivalentTo(position))
				Expect(r.Face).Should(BeEquivalentTo(face))
				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

			It("should be able to spin looplessly to the RIGHT", func() {

				name := "tmp/canSpin.txt"
				body := "PLACE 0,0,NORTH \n RIGHT \n RIGHT \n  RIGHT \n  RIGHT \n  RIGHT \n"
				createTheInputFile(name, body)

				position := map[string]int{"x": 0, "y": 0}
				face := "EAST"
				ready := true

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)
				Expect(r.Position).Should(BeEquivalentTo(position))
				Expect(r.Face).Should(BeEquivalentTo(face))
				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})
		})

		Context("moving the robot", func() {

			It("should be able to move (1 unit per time) not further than the border of the gameboard (5 unit)", func() {

				name := "tmp/canMove.txt"
				body := "PLACE 0,0,NORTH \n MOVE \n MOVE \n MOVE \n MOVE \n  MOVE \n MOVE \n  MOVE \n  MOVE \n"
				createTheInputFile(name, body)

				ready := true

				f := readTheInputFile(name)
				defer f.Close()

				r := Simulation(f)
				Expect(r.Ready).Should(BeEquivalentTo(ready))
			})

		})

		Context("asked to report its position", func() {
			It("should print a report into the console", func() {

				name := "tmp/canReport.txt"
				body := "PLACE 0,0,NORTH \n REPORT"
				createTheInputFile(name, body)

				f := readTheInputFile(name)
				defer f.Close()

				r, w, _ := os.Pipe()
				tmp := os.Stdout
				defer func() {
					os.Stdout = tmp
				}()

				os.Stdout = w

				go func() {
					Simulation(f)
					w.Close()
				}()

				stdout, _ := ioutil.ReadAll(r)
				Expect(strings.TrimSpace(string(stdout))).To(Equal("0,0,NORTH"))
			})

		})

		AfterSuite(func() {
			log.Println("Removing all the text - testdata - files in tmp")
			os.Remove("tmp/canBePlaced.txt")
			os.Remove("tmp/canSpin.txt")
			os.Remove("tmp/canMove.txt")
			os.Remove("tmp/canReport.txt")
		})
	})

})

// Three little servos meant to
func createTheInputFile(name string, body string) {
	f, err := os.Create(name)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString(body)
}

// and to
func readTheInputFile(f string) *os.File {
	file, err := os.Open(f)
	if err != nil {
		log.Fatal(err)
	}
	return file
}

// and to
func captureLogOutput(f func()) string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	f()
	log.SetOutput(os.Stderr)
	return buf.String()
}
