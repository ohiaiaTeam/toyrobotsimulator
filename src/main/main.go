package main

import (
	"flag"
	"log"
	"os"
	. "simulator"
)

func main() {
	// Using Flags
	fileFlag := flags()
	log.Println("Input file in use is: ", *fileFlag)

	// Opening the input File
	file := fileLookFor(*fileFlag)

	// Deferred Close. This is a quick/dirty exception
	// It's idiomatic to defer a `Close` immediately
	// after opening a file.
	defer file.Close()

	// Launching the Simulation
	Simulation(file)
}

// Flags allowed to the command line
func flags() *string {

	// -file => will open the default file if no other file is specified
	filePtr := flag.String("file", "public/default.txt", "public")

	flag.Parse()
	return filePtr
}

// Manage the opening of the input file
func fileLookFor(f string) *os.File {
	// Open/Error the file
	file, err := os.Open(f)

	// Error & Exit
	if err != nil {
		log.Fatal(err)
	}

	return file
}
