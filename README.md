# My approach to the challenge

## Said that:
I do not consider myself a CodeMonkey and that I do really hate the idea of an evaluation test ( as I've known too many people unable to perform well during the challenge and that are GREAT coders/developers and studying psychology I found myself right in few more intuition about why … ) I have to admit I quite enjoyed this codeChallenge.

The logical problem itself was quite easy and I can find an (implemented) use thinking in a motorization for a/my telescope.  (Another project to add to my RaspberryPI scrum board.) That will probably explain why I choose some solution and not another. Or what I would like to change and implement in the future.

And I also thought that doing it with GoLang could be a nice entertaining way to test and display (and improve some how) my Go Lang abilities. 

The code as it is now is quite sufficient and of course it can be improved significantly. 
But I’m running out time and the code itself is at an acceptable level as to go to production.

## Improvements:

### Add:
* a flag for a more verbose, and a really more verbose output
* all the missing unit tests 
* some behaviour test to the main package
* some deploy tool (docker and ansible…) and infrastructure ( es development env. with Vagrant)
* docs and more comments to the code (the code complete a really esay task and is auto documented by the behavoiur test right now)

### Changes:
* the rotation system from “cardinal names” to numbers to work more precisely (e.g. with degrees in the telescope project mentioned above )
* probably using channels and upgrading the software to satisfy my curiosity as much as possible.

## Time:
My personal free time to work on the software was really limited by my actual situation as “happy father waiting for his third child to come to this world and supporting his wife and other kids”, working as Freelance, studying ... and trying to not go crazy or die in the intent ... 

So I wrote this challenge across 1 week “everywhere and whenever was possible” ... If you sum every time-cut you can say I wrote it in approximately 20 hours. Probably a less fragmented use of the time would allow me to write something similar in a significantly smaller amount of time.


## My expectation:
I’m looking forward to receive your feedback on how to improve my GO lang skills 
and to talk a bit more with you about the role you are looking for to “fill”.

_____________________________________________________________________

# How to use it:

I realized that I didn't built the package and always used a command like 

```
#!bash

$ go run src/main/*.go 
```


So something like 

```
#!bash

$ go run src/main/*.go -file=public/demo1.txt

```
will run the first input example given in the cahllenge description

## test it

```
#!bash

$ cd src/simulator_test
$ ginkgo
```

_____________________________________________________________________


# Challenge Original Description



```
#!txt

Toy Robot Simulator
--------------------
Create an application that can read in commands of the following form:

PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT

. The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
. There are no obstructions on the table surface.
. The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still be allowed.
. PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
. The origin (0,0) can be considered to be the SOUTH WEST most corner.
. The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
. MOVE will move the toy robot one unit forward in the direction it is currently facing.
. LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.
. REPORT will announce the X,Y and orientation of the robot.
. A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.
. The robot must not fall off the table during movement. This also includes the initial placement of the toy robot. Any move that would cause the robot to fall must be ignored.

Example Input and Output:
a)
PLACE 0,0,NORTH
MOVE
REPORT
Output: 0,1,NORTH

b)
PLACE 0,0,NORTH
LEFT
REPORT
Output: 0,0,WEST

c)
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
Output: 3,3,NORTH

- There must be a way to supply the application with input data.
- Provide test data to exercise the application.
- The application must run and you should provide sufficient evidence that your solution is complete by, as a minimum, indicating that it works correctly against the supplied test data.
- The submission should be production quality and it can be done in any language (using JavaScript, Ruby or Go would be a bonus).
- The code should be original and you may not use any external libraries or open source code to solve this problem, but you may use external libraries or tools for building or testing purposes. Specifically, you may use unit testing libraries or build tools available for your chosen language.
```